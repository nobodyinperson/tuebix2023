# My Tübix2023 Slides

These are the sources for my presententations at the [Tübix2023 Linux Day in Tübingen on 01.07.2023](https://tuebix.org).

To access the resource files (PDFs, images, etc.), install [git-annex](https://git-annex.branchable.com) and run:

```bash
git clone https://gitlab.com/nobodyinperson/tuebix2023 && cd tuebix2023
git annex assist # or 'git annex sync --content' if your git-annex is too old
```
