% written by Yann Büchau
% https://gitlab.com/tue-umphy/templates/EKUT-beamer-theme

\documentclass[
	aspectratio=169 % 169 = 16:9 ratio, 43 = 4:3 ratio
	,c % center frame content vertically
	,shrink % shrink frame content if necessary
  ,8pt
]{beamer} % this document is a beamer presentation

\usetheme{EKUT-slides} % use the EKUT slide theme

\usepackage[british]{babel} % localisation
\usepackage{graphicx} % be able to include graphics
\usepackage{listings}

\let\tempone\itemize
\let\temptwo\enditemize
\renewenvironment{itemize}{\tempone\addtolength{\itemsep}{0.5\baselineskip}}{\temptwo}


%      __  __      _            _       _        
%     |  \/  | ___| |_ __ _  __| | __ _| |_ __ _ 
%     | |\/| |/ _ \ __/ _` |/ _` |/ _` | __/ _` |
%     | |  | |  __/ || (_| | (_| | (_| | || (_| |
%     |_|  |_|\___|\__\__,_|\__,_|\__,_|\__\__,_|
%                                          
\title[Git Annex]{\mbox{Git Annex: Syncing, Archiving and Managing Files like a Pro}}
\input{../metadata.tex}
\session{(\faIcon{microphone} English Re-Recording)\\[0.5em]Workshops -- 10:00-11:50 -- W1/C215}
\institute[]{}
\address{}
\telephone{}
\rightheaderlogo{git-annex-logo}
\titlecontent{
  \includegraphics[width=0.5\linewidth,height=0.4\textheight,keepaspectratio]{git-annex-logo-large}
  \\{\footnotesize\faIcon{image}~\href{https://git-annex.branchable.com/logo}{git-annex.branchable.com}}
}

%    _____ _                _____            _             
%   |  ___(_)_ __   ___    |_   _|   _ _ __ (_)_ __   __ _ 
%   | |_  | | '_ \ / _ \_____| || | | | '_ \| | '_ \ / _` |
%   |  _| | | | | |  __/_____| || |_| | | | | | | | | (_| |
%   |_|   |_|_| |_|\___|     |_| \__,_|_| |_|_|_| |_|\__, |
%                                                    |___/ 
\setlength\titlelinesep{0.5em} % Adjust line spacing in title

\setbeamercolor{title}{fg=ekut-red}

\begin{document}


%    _____ _                       _               _                   _             _   
%   |_   _| |__   ___    __ _  ___| |_ _   _  __ _| |   ___ ___  _ __ | |_ ___ _ __ | |_ 
%     | | | '_ \ / _ \  / _` |/ __| __| | | |/ _` | |  / __/ _ \| '_ \| __/ _ \ '_ \| __|
%     | | | | | |  __/ | (_| | (__| |_| |_| | (_| | | | (_| (_) | | | | ||  __/ | | | |_ 
%     |_| |_| |_|\___|  \__,_|\___|\__|\__,_|\__,_|_|  \___\___/|_| |_|\__\___|_| |_|\__|
%                                                                                    

\begin{frame}[plain]
	\titlepage
\end{frame}

\begin{frame}[c]{\contentsname}
	\centering
	\tableofcontents
\end{frame}

\begin{frame}[c]{Origins of Git Annex}
  \section{\insertframetitle}
  \begin{columns}
    \begin{column}{0.35\linewidth}
      \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{joey-hess}
      \centering
      \faIcon{image}~\href{https://lwn.net/Articles/713653/}{lwn.net}
    \end{column}
    \begin{column}{0.65\linewidth}
      \begin{itemize}
        \item \textbf{Joey Hess}
        \item Longtime Debian developer
        \item Lives in a cabin in the woods with solar power, spring water and (previously) slow internet
        \item Needed a way to batch-transfer files when he has fast internet, e.g. in the city
        \item Needed an overview of which files he had already saved where to avoid unnecessary, slow downloads
        \item[$\implies$] developed Git Annex
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{What can you do with Git Annex?}
  \section{What can Git Annex do?}
  \begin{itemize}
    \item Synchronize folders similarly to Nextcloud Files, Syncthing, Dropbox, etc.
    \item Include large/binary/sensitive files in a \texttt{git} repository
    \item Simplify the pull/commit/push \texttt{git} workflow hassle (just run \texttt{git annex assist})
    \item Keep the complete change history of any files
    \item Perform redundant backups of files and keep an overview
    \item Quickly delete files that are safely stored elsewhere
    \item Catalog/synchronize/secure research data
    \item Catalog/synchronize/archive a media collection
    \item works under Linux/Mac/Windows, even Android (Termux)
    \item \dots
  \end{itemize}
\end{frame}

\begin{frame}{What can Git Annex \emph{not} do (well)?}
  \begin{itemize}
    \item From about \textbf{100000 files} in the repository it gets slow
    \item It's no fun on \textbf{slow USB sticks} and bad file systems (e.g. FAT)
    \item Simple \textbf{graphical interfaces} are missing
    \item Git Annex does \emph{not} arrange for the actual \textbf{connection of repositories}. You have to establish connections between PCs etc. yourself (unlike Syncthing, for example)
    \item Git Annex has \textbf{no permissions management}. Anyone who has the repository and push permission can do anything.
    \item \textbf{learning curve}
    \item \dots ?
  \end{itemize}
\end{frame}

\begin{frame}{What is Git Annex? - Overview}
  \section{What is Git Annex?}
  \begin{itemize}
    \item \textbf{extension} for version control system \href{https://git-scm.org}{\texttt{git}}
    \item \texttt{git} is \textbf{toll} for text files (code, etc.)
    \item \texttt{git} is \textbf{bad} for everything else (photos, videos, music, data, archives, spreadsheets, etc.), especially if they change
     \begin{itemize}
        \item For binary files, \texttt{git} saves the \textbf{complete content of each version} quasi-\textbf{inerasable} in the version history
        \item The repository becomes huge and slow
     \end{itemize}
     \item Git Annex teaches \texttt{git} not to track the \emph{content} of certain files, but only \emph{metadata}
     \item Git Annex remembers which \emph{repositories}/PCs contain what \emph{contents}
     \item Git Annex uses \texttt{git} to synchronize the metadata, various protocols for the content
  \end{itemize}
\end{frame}

\begin{frame}{How does Git Annex work? The \texttt{git-annex} branch}
  \begin{itemize}
    \item Git Annex maintains a \texttt{git-annex} branch in the background. It contains:
    \begin{itemize}
      \item global \textbf{settings} (\texttt{git annex config})
      \item \textbf{descriptions} of each known repository (harddisk1, laptop2, server3, etc.) (\texttt{git annex info})
      \item Which files does each repository \textbf{prefer} want? (\texttt{git annex wanted})
      \item \textbf{Logbook}: In which repository($\hat{=}$data container) is which file? (\mbox{\texttt{git annex log|whereis|list|etc.}})
      \item Arbitrary \textbf{metadata} (notes, etc.) for files (\texttt{git annex metadata})
    \end{itemize}
    \item The \texttt{git-annex} branch is designed so that it can be merged without conflicts
  \end{itemize}
\end{frame}

\begin{frame}{Git Annex workflow for a simple, shared folder}
  \section{\insertframetitle}
  \begin{itemize}
    \item An annex can be added to any \texttt{git} repository:
  \end{itemize}
  \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{git-init-git-annex-init}
\end{frame}

\begin{frame}{Git Annex workflow for a simple, shared folders}
  \begin{itemize}
    \item Edit/add files, then commit (and synchronize) with \texttt{git annex assist}
    \item \texttt{git annex assist} runs \texttt{git~annex~add, git~add, git~commit, git~pull, git~merge, git~push} and copies files around if necessary
  \end{itemize}
  \includegraphics[width=\linewidth,height=0.7\textheight,keepaspectratio]{git-annex-assist-with-two-basic-files}
\end{frame}

\begin{frame}{What is Git Annex committing?}
  \begin{itemize}
    \item Git Annex has turned the files into \textbf{symbolic links}
    \item The links point to the hidden folder under \texttt{.git/annex/objects}
  \end{itemize}
  \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{git-annex-link-locked-files}
  \begin{itemize}
    \item Files are stored there in such a way that they \textbf{cannot be deleted or edited}
  \end{itemize}
  \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{git-annex-objects-tree-minus-l}
\end{frame}

\begin{frame}{But I want to be able to edit the files! I don't like links!}
  \begin{itemize}
    \item Files can be unlocked with \texttt{git annex unlock} and \textbf{become normal files}
  \end{itemize}
  \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{git-annex-unlock-and-ls}
\end{frame}

\begin{frame}{But I want to be able to edit the files! I don't like links!}
  \begin{itemize}
    \item If new files should also remain editable: \texttt{git annex config --set annex.addunlocked true}
  \end{itemize}
  \includegraphics[width=\linewidth,height=0.8\textheight,keepaspectratio]{git-annex-addunlocked-newfile-and-ls}
\end{frame}


\begin{frame}{The good/the bad -- Locked/Unlocked}
  \begin{columns}
    \begin{column}{0.5\linewidth}
      \Large \faIcon{lock} Locked (Symlink)
      \vspace{2em}
      \begin{itemize}
        \item[\faIcon{thumbs-down}] not editable
        \item[\faIcon{thumbs-down}] some programs do not like symlinks
        \item[\faIcon{thumbs-up}] identical files copied several times in the repository only require storage space \textbf{once}
      \end{itemize}
    \end{column}
    \begin{column}{0.5\linewidth}
      \Large \faIcon{lock-open} Unlocked (normal file)
      \vspace{2em}
      \begin{itemize}
        \item[\faIcon{thumbs-up}] editable
        \item[\faIcon{thumbs-up}] works with all programs
        \item[\faIcon{thumbs-down}] each copy in the repository requires memory plus the original file in \texttt{.git/annex/objects}
      \end{itemize}
    \end{column}
  \end{columns}
  \vspace{2em}
  \begin{itemize}
    \item You can also define automatic rules
    \item e.g. \texttt{.ods} files are always unlocked:
    \item \lstinline{git annex config --set annex.addunlocked 'include=*.ods'}
  \end{itemize}
\end{frame}


\begin{frame}{Synchronize!}
  \begin{itemize}
    \item To synchronize files and metadata, \emph{remotes} must be added
    \begin{itemize}
      \item \textbf{Normal \texttt{git} remotes} such as GitLab, GitHub (but only for metadata) or your own \texttt{git} server with Git Annex installed
      \item \textbf{Special remotes}: without metadata, only file content, various options
      \begin{itemize}
        \item adb, Amazon Glacier, bittorrent, bup, ddar, directory, gcrypt, git-lfs, hook, rsync, S3, tahoe, tor, web, webdav, git, httpalso, borg, xmpp
        \item GPG encryption and chunking for file size obfuscation possible
        \item protocol for own remote types
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Which remotes are configured?}
  \includegraphics[width=\linewidth,height=0.8\textheight,keepaspectratio]{git-annex-info-only-here}
\end{frame}

\begin{frame}{Describe repositories for overview}
  \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{git-annex-describe-here-and-info}
\end{frame}

\begin{frame}{Connecting archive drives}
  \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{git-remote-add-and-assist}
\end{frame}

\begin{frame}{Connecting archive data drives}
  \begin{itemize}
    \item The drive is now logged in the \texttt{git-annex} branch
  \end{itemize}
  \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{git-annex-info-after-remote-add}
\end{frame}

\begin{frame}{Which files are located where?}
  \begin{itemize}
    \item There is nothing on the hard disk yet
  \end{itemize}
  \includegraphics[width=\linewidth,height=0.8\textheight,keepaspectratio]{git-annex-list-nothing-on-hdd}
\end{frame}

\begin{frame}{Synchronize files!}
  \begin{itemize}
    \item The files are copied to the hard disk with \texttt{git annex assist}
  \end{itemize}
  \includegraphics[width=\linewidth,height=0.8\textheight,keepaspectratio]{git-annex-assist-copy-to-hdd}
\end{frame}

\begin{frame}{Which files are located where?}
  \begin{itemize}
    \item Now the files are on the hard disk
  \end{itemize}
  \includegraphics[width=\linewidth,height=0.8\textheight,keepaspectratio]{git-annex-list-now-on-hdd}
\end{frame}


\begin{frame}{Further possibilities}
  \begin{itemize}
    \item Add a server instead of a hard disk (\texttt{git remote add myserver ssh://...})
    \item Delete all files from the hard disk(\texttt{\lstinline{git annex drop --from=usb4TB}})
    \item Move all files to the hard disk(\texttt{\lstinline{git annex move --to=usb4TB}})
    \item Require that there are at least 2 copies (then Git Annex refuses the two commands above) (\texttt{git annex numcopies 2})
    \item Assign metadata to files(\texttt{\lstinline{git annex metadata --set notiz='wichtig!' bla.md}})
    \item Query metadata(\texttt{\lstinline{git annex metadata bla.md}})
    \item Create folder structure according to metadata, e.g. in music collection first level the artist, then the album(\texttt{\lstinline{git annex view artist='*' album='*'}})
    \item Specify that the hard disk only wants \texttt{.md} files -- everything else will then be removed from there (\texttt{\lstinline{git annex wanted usb4TB 'include=*.md'}})
    \item \dots
  \end{itemize}
\end{frame}

\begin{frame}{Mixed repositories}
  \begin{itemize}
    \item Have some files tracked by \texttt{git}, others with Git Annex
  \end{itemize}
  \includegraphics[width=\linewidth,height=0.8\textheight,keepaspectratio]{git-attributes-largefiles}
\end{frame}

\begin{frame}{Really remove unused/deleted files}
  \begin{itemize}
    \item Find and (re)move annex files no longer appearing in the current \texttt{git}-tree
  \end{itemize}
  \includegraphics[width=\linewidth,height=0.8\textheight,keepaspectratio]{git-annex-drop-unused}
\end{frame}

\begin{frame}{Git Annex Assistant}
  \section{\insertframetitle}
  \begin{itemize}
    \item Web interface for background synchronization à la Syncthing/Nextcloud/Dropbox
  \end{itemize}
  \includegraphics[width=\linewidth,height=0.8\textheight,keepaspectratio]{git-annex-assistant}
\end{frame}

\begin{frame}{What does the Git Annex Assistant do?}
  \begin{columns}
    \begin{column}{0.6\linewidth}
      \begin{itemize}
        \item Runs in the background, autostart possible
        \item Basically an infinite loop \texttt{git annex assist} (only more efficient)
        \item Adds all files \texttt{unlocked}
        \item Unfavorable for git-tracked merge conflicts, so best only for pure annex repos
        \item Small, simple GUI for managing the \textbf{remotes}, no file management
      \end{itemize}
    \end{column}
    \begin{column}{0.4\linewidth}
      \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{git-annex-assistant}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{Further material}
  \section{\insertframetitle}
  \begin{itemize}
    \item Git Annex \textbf{Homepage}: \url{https://git-annex.branchable.com}
    \item \textbf{DataLad} -- Research data management based on Git Annex: \url{https://datalad.org}
    \item \textbf{distribits} conference April 4th - April 6th, 2024
    \begin{itemize}
      \item topics: distributed data management, DataLad, Git Annex, etc.
      \item \url{https://distribits.live}
    \end{itemize}
    \item my \textbf{Thunar plugin} for Git Annex: \url{https://pypi.org/project/thunar-plugins}
    \item WIP: my Git Annex \textbf{Control Center TUI}: \url{https://gitlab.com/nobodyinperson/git-annex-control-center}
  \end{itemize}
\end{frame}

\begin{frame}{Git Annex Thunar Plugin}
  \section{\insertframetitle}
  \begin{columns}
    \begin{column}{0.55\textwidth}
      \url{https://pypi.org/project/thunar-plugins}
      \\[2em]
      \includegraphics[width=\linewidth, height=\textheight,keepaspectratio]{git-annex-thunar-plugin-properties}
    \end{column} 
    \begin{column}{0.45\textwidth}
      \includegraphics[width=\linewidth, height=\textheight,keepaspectratio,trim={0 0 1.2cm 2cm},clip]{git-annex-thunar-plugin-view-context-menu}
    \end{column} 
  \end{columns}
\end{frame}

\begin{frame}{WIP: Git Annex Control Center TUI}
  \section{\insertframetitle}
  \centering
  \url{https://gitlab.com/nobodyinperson/git-annex-control-center}
  \\[1em]
  Try it with \href{https://nixos.org}{nix}: \texttt{> nix run gitlab:nobodyinperson/git-annex-control-center}
  \\[1em]
  \includegraphics[width=\linewidth, height=0.8\textheight,keepaspectratio]{git-annex-control-center}
\end{frame}

\begin{frame}{Contact}
  \begin{columns}
    \begin{column}{0.6\linewidth}
      \contact
    \end{column}
    \begin{column}{0.4\linewidth}
      \hfill\includegraphics[width=0.5\linewidth,height=0.5\textheight,keepaspectratio]{yann-buechau}
    \end{column}
  \end{columns}
\end{frame}

\end{document}
