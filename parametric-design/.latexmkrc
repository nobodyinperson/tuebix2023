use Cwd qw/cwd/;

# Add the current working directory to the TEXINPUTS
$ENV{'TEXINPUTS'}= cwd . "/../EKUT-beamer-theme/EKUT//:$ENV{TEXINPUTS}";
$ENV{'TEXINPUTS'}= cwd . "/../EKUT-beamer-theme/cc//:$ENV{TEXINPUTS}";
$ENV{'TEXINPUTS'}= cwd . "/../pic//:$ENV{TEXINPUTS}";

# Search the bibliography wherever standard LaTeX files are
$ENV{'BIBINPUTS'}=$ENV{'TEXINPUTS'};
$ENV{'BSTINPUTS'}=$ENV{'BIBINPUTS'};

$pdflatex = join " ",
    "pdflatex" ,
    "-interaction nonstopmode" ,
    "-halt-on-error",
    "-file-line-error %O %S";

# open the PDF with the standard PDF viewer
$pdf_previewer = 'start xdg-open';
$use_make_for_missing_files = 1;
$make = "git annex get";
$preview_continuous_mode = 1;
